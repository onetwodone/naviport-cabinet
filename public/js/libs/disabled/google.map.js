function initialize() {

  var centered     = {lat: 59.9349192, lng: 30.3242834};

  var mebelsity    = {lat: 59.9909849, lng: 30.2493739};
  var akvilon      = {lat: 59.9830927, lng: 30.3559573};
  var akvilon2     = {lat: 59.9817097, lng: 30.3561423};
  var continent    = {lat: 59.8814367, lng: 30.3085393};

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 11,
      center: centered,
      disableDefaultUI: true,
      disableDoubleClickZoom: true,
      scrollwheel: false,
      mapTypeIds: [google.maps.MapTypeId.ROADMAP ]
    });

    var map = new google.maps.Map(document.getElementById("map"),map);

    var image = {
      url: 'img/icons/icon__marker.png',
      size: new google.maps.Size(36, 47),
      origin: new google.maps.Point(0,0),
      anchor: new google.maps.Point(0, 32)
    }

    var marker = new google.maps.Marker({
      position: mebelsity,
      map: map,
      title: 'Мебельный салон Диваним в Мебель Сити',
      icon: image
    });

    var marker = new google.maps.Marker({
      position: akvilon,
      map: map,
      title: 'Мебельный салон Диваним в Аквилоне',
      icon: image
    });

     var marker = new google.maps.Marker({
      position: akvilon2,
      map: map,
      title: 'Мебельный салон Диваним в Аквилоне',
      icon: image
    });

     var marker = new google.maps.Marker({
      position: continent,
      map: map,
      title: 'Мебельный салон Диваним в Мебельном Континенте',
      icon: image
    });

  }

 // function newLocation(newLat,newLng) {
 //    map.setCenter({
 //        lat : newLat,
 //        lng : newLng
 //    });
// }

google.maps.event.addDomListener(window, 'load', initialize);

// $(document).ready(function(){


//    $(".address-b__link").on('click', function (e) {
//      e.preventDefault();
//       newLocation(55.749792,37.632495);
//     });
// });