// DOM Ready

$(document).ready(function(){

	// Custom Scrollbar

 		  $('.scrollbar').scrollbar();

	// Smooth Scrolling

		try {
			$.browserSelector();
			if($("html").hasClass("chrome")) {
				$.smoothScroll();
			}
		} catch(err) {};


	$('.checkbox__call').on('click', function() {
		 $('#checkbox__call').slideToggle();
	});

	$('.warning .exit').on('click', function() {
		 $('.warning').slideUp();
	});

});


// Drag object fix

	$("img, a").on("dragstart", function(e) {
		e.preventDefault();
	});

// Console test

	$(document).ready(function(){
		console.log('js/jquery.functions.js was connected!');
	});

	if (window.jQuery) {
		var jQueryVersion = jQuery.fn.jquery
		console.log('js/jquery.min.js ('+ jQueryVersion + ') was connected!');
	};
